#!/bin/sh
FOLDER="$1 at `date`";
mkdir "$FOLDER";
ABOUTME="$FOLDER/About_me";
MYFRIENDS="$FOLDER/My_friends";
MYSYSTEMINFO="$FOLDER/My_system_info";
mkdir "$ABOUTME" "$MYFRIENDS" "$MYSYSTEMINFO";

mkdir "$ABOUTME/personal";
touch "$ABOUTME/personal/facebook.txt";
echo "https://www.facebook.com/$2">"$ABOUTME/personal/facebook.txt";


mkdir "$ABOUTME/professional";
touch "$ABOUTME/professional/linkedin.txt";
echo "https://www.linkedin.com/in/$3">"$ABOUTME/professional/linkedin.txt";


touch "$MYFRIENDS/list_of_my_friends.txt";
curl "https://gist.github.com/tegarimansyah/e91f335753ab2c7fb12815779677e914" > "$MYFRIENDS/list_of_my_friends.txt";


touch "$MYSYSTEMINFO/about_this_laptop.txt";
echo "My username:$1\nWith host: $(uname -a)" > "$MYSYSTEMINFO/about_this_laptop.txt";

touch "$MYSYSTEMINFO/internet_connection.txt";
ping -c 3 google.com > "$MYSYSTEMINFO/internet_connection.txt";

